﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTestIoC
{
    public class ReportFactory
    {
        public static Reports.Report GetReport(int id, List<Reports.ReportParameters> param, Reports.ReportTypes types)
        {
            switch (types)
            {
                case Reports.ReportTypes.Excel:
                    return new Reports.ExcelReport(new Managers.ExcelManager());
                    break;
                case Reports.ReportTypes.QueryReport:
                    return null;
                    break;
                case Reports.ReportTypes.TestReport:
                    return null;
                    break;
            }
            return null;
        }
    }
}
