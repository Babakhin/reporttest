﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTestIoC.Managers
{
    public interface IReportManager
    {
        Stream GetReport(Reports.Report report);

        string TestReport(Reports.Report report);
    }
}
