﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleAppTestIoC.Reports;
using ConsoleAppTestIoC.Parameters;
using ConsoleAppTestIoC.Managers;

namespace ConsoleAppTestIoC
{
    class Program
    {
        static void Main(string[] args)
        {
            Report r = ReportFactory.GetReport(1, null, ReportTypes.Excel);
            r.Generate();
            Console.ReadLine();
        }
    }
}
