﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTestIoC.Reports
{
    public enum ReportTypes
    {
        Excel, QueryReport, TestReport
    }

    public enum ParameterDataTypes
    {
        Int, Double, Date, DateTime, Time, String
    }
}
