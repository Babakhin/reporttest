﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTestIoC.Reports
{
    public class ReportParameters
    {
        public virtual string Name { get; set; }

        public virtual ParameterDataTypes DataType { get; set; }

        public virtual object Value { get; set; }
    }
}
