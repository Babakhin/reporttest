﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTestIoC.Reports
{
    public abstract class Report
    {
        public string Name { get; set; }

        public IList<ReportParameters> Parameters { get; set; }

        public ReportTypes ReportType { get; set; }

        private Managers.IReportManager _manager { get; set; }

        public Report(Managers.IReportManager manager)
        {
            _manager = manager;
        }

        public void Generate()
        {
            _manager.GetReport(this);
        }
    }
}
